#ifndef EVYL_CORE_SURFACE_RESULT
#define EVYL_CORE_SURFACE_RESULT

#include <evyl/core/tools/common.hpp>

namespace EVY_NAMESPACE {
    enum EVY_RESULT_ENUM_NAME(Surface) : uint8_t
    {
        eSurfaceResultSuccess             = 0,
        eSurfaceResultError               = 1,
        eSurfaceResultInvalidWindow       = 2,
        eSurfaceResultInvalidInstance     = 3,
        eSurfaceResultDevicePickingFailed = 4,
    };

    EVY_CREATE_ERROR_CATEGORY_HPP(Surface);
} // namespace EVY_NAMESPACE

#endif