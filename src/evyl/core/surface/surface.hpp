#ifndef EVYL_CORE_SURFACE_SURFACE
#define EVYL_CORE_SURFACE_SURFACE

#include "queueFamily.hpp"
#include "result.hpp"

#include <evyl/core/instance/instance.hpp>
#include <evyl/core/window/window.hpp>

#include <set>

namespace EVY_NAMESPACE {
    class Surface final :
        public StaticConstructible<Surface>,
        protected Needs<Window>,
        protected Needs<Instance> {
        private:
        VkSurfaceKHR surface;

        public:
        /**
         * @brief Construct a new Surface object
         *
         * @param window the window to create the surface for
         * @param instance the instance to create the surface on
         */
        Surface(const shared<Window>& window, const shared<Instance>& instance);

        /**
         * @brief Destroy the Surface object.
         *
         * Internally calls vkDestroySurface
         */
        ~Surface();

        /**
         * @brief Simple conversion function to get the surface
         *
         * @return VkSurfaceKHR
         */
        inline operator VkSurfaceKHR() const noexcept { return surface; }

        /**
         * @brief Pick a physical device based on needed extensions and indices
         *
         * @param extensions the requested extensions
         * @return pair of physical device and its queue family indices. Wrapped in Result<> so
         * check before usage.
         */
        Result<std::pair<VkPhysicalDevice, QueueFamilyIndices>>
        pickPhysicalDevice(const std::span<const char* const>& extensions) const noexcept;
    };
} // namespace EVY_NAMESPACE

#endif