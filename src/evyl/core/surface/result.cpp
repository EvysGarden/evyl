#include "result.hpp"

namespace EVY_NAMESPACE {
    EVY_CREATE_ERROR_CATEGORY_CPP(Surface) {
        switch (CODE) {
            case eSurfaceResultSuccess: return "No problems detected";
            case eSurfaceResultError: return "A general error has occured";
            case eSurfaceResultInvalidWindow: return "The Window pointer is invalid";
            case eSurfaceResultInvalidInstance: return "The Instance pointer is invalid";
            case eSurfaceResultDevicePickingFailed: return "Couldn't pick a physical device";
            default: return "An unknown error has occured";
        }
    }
} // namespace EVY_NAMESPACE