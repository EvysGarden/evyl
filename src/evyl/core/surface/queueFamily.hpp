#ifndef EVYL_CORE_SURFACE_QUEUE_FAMILY
#define EVYL_CORE_SURFACE_QUEUE_FAMILY

#include <evyl/core/tools/common.hpp>

#include <optional>

namespace EVY_NAMESPACE {

    /**
     * @brief
     *
     */
    struct QueueFamilyIndices final {
        std::optional<uint32_t> graphics;
        std::optional<uint32_t> compute;
        std::optional<uint32_t> transfer;
        std::optional<uint32_t> present;

        QueueFamilyIndices() = default;

        QueueFamilyIndices(VkPhysicalDevice device, VkSurfaceKHR surface);

        bool
        complete() const noexcept;
    };
} // namespace EVY_NAMESPACE

#endif