#include "queueFamily.hpp"

namespace EVY_NAMESPACE {
    QueueFamilyIndices::QueueFamilyIndices(VkPhysicalDevice device, VkSurfaceKHR surface) {
        uint32_t familyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(device, &familyCount, nullptr);
        std::vector<VkQueueFamilyProperties> families(familyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &familyCount, families.data());

        for (uint32_t i = 0; i < familyCount; ++i) {
            if (families.at(i).queueFlags & VK_QUEUE_GRAPHICS_BIT) graphics = i;
            if (families.at(i).queueFlags & VK_QUEUE_COMPUTE_BIT) compute = i;
            if (families.at(i).queueFlags & VK_QUEUE_TRANSFER_BIT) transfer = i;

            auto presentable = VK_FALSE;
            if (auto result
                = vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentable)) {
                throw MonoException(make_error_code(result));
            }
            if (presentable) present = i;
            if (complete()) return;
        }
    }

    bool
    QueueFamilyIndices::complete() const noexcept {
        return graphics.has_value() && compute.has_value() && transfer.has_value()
               && present.has_value();
    }
} // namespace EVY_NAMESPACE