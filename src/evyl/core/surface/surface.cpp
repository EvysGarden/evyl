#include "surface.hpp"

namespace EVY_NAMESPACE {
    Surface::Surface(const shared<Window>& window, const shared<Instance>& instance) :
        Needs<Window>(window), Needs<Instance>(instance) {
        if (!window.use_count()) {
            throw MonoException(make_error_code(eSurfaceResultInvalidWindow));
        }
        if (!instance.use_count()) {
            throw MonoException(make_error_code(eSurfaceResultInvalidInstance));
        }

        if (auto result = glfwCreateWindowSurface(*instance, *window, nullptr, &surface)) {
            throw MonoException(make_error_code(result));
        }
    }

    Surface::~Surface() { vkDestroySurfaceKHR(*Needs<Instance>::res, surface, nullptr); }

    Result<std::pair<VkPhysicalDevice, QueueFamilyIndices>>
    Surface::pickPhysicalDevice(const std::span<const char* const>& extensions) const noexcept {
        auto devices = Needs<Instance>::res->queryPhysicalDevices();

        /*
         * Lambda to check for extension support on a device
         */
        auto complient = [&](VkPhysicalDevice device) {
            uint32_t deviceExtensionCount = 0;
            if (auto result = vkEnumerateDeviceExtensionProperties(
                    device, nullptr, &deviceExtensionCount, nullptr)) {
                throw MonoException(make_error_code(result));
            }
            std::vector<VkExtensionProperties> deviceExtensions(deviceExtensionCount);
            vkEnumerateDeviceExtensionProperties(
                device, nullptr, &deviceExtensionCount, deviceExtensions.data());

            std::set<std::string> requestedExtensions(extensions.begin(), extensions.end());
            for (const auto& deviceExtension : deviceExtensions) {
                requestedExtensions.erase(std::string(deviceExtension.extensionName));
            }

            return requestedExtensions.empty();
        };

        for (const auto& device : devices) {
            try {
                if (complient(device)) {
                    QueueFamilyIndices families { device, surface };
                    if (families.complete()) return std::move(std::pair(device, families));
                }
            } catch (const MonoException<std::error_code>& err) { return { err }; }
        }

        return { make_error_code(eSurfaceResultDevicePickingFailed) };
    }
} // namespace EVY_NAMESPACE