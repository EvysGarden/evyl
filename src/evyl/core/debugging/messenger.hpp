#ifndef EVYL_CORE_DEBUGGING_MESSENGER
#define EVYL_CORE_DEBUGGING_MESSENGER

#include <evyl/core/tools/config.hpp>

#include <cassert>
#include <span>
#include <vector>
#include <vulkan/vulkan_core.h>

namespace EVY_NAMESPACE {
    /**
     * @brief Populates a debug messenger creation info
     *
     * @param info the info struct to populate.
     * @param severity the severity of the messages to display.
     * @param types the types of messages to display.
     * @param userData a pointer to user-specific data.
     * @param userCallback the callback to call every time there is something to show.
     *
     * @note the severity is to be understood as a minimal. SEVERITY_VERBOSE will include
     * all other SEVERITY bits while SEVERITY_ERROR won't include any other SEVERITY bits.
     *
     * @return the given info pointer or nullptr if pointer was nullptr.
     */
    VkDebugUtilsMessengerCreateInfoEXT*
    populateDebugMessengerInfo(VkDebugUtilsMessengerCreateInfoEXT*                     info,
                               VkDebugUtilsMessageSeverityFlagsEXT                     severity,
                               const std::span<const VkDebugUtilsMessageTypeFlagsEXT>& types,
                               void*                                                   userData,
                               PFN_vkDebugUtilsMessengerCallbackEXT userCallback);

    /**
     * @brief Simple callback that displays messages in colors and with time through fmt::chrono
     * and fmt::color.
     * @note for the parameters and return, read populateDebugMessengerInfo and/or vulkan docs.
     */
    VKAPI_ATTR VkBool32 VKAPI_CALL
    debugMessengerCallback(VkDebugUtilsMessageSeverityFlagBitsEXT      severity,
                           VkDebugUtilsMessageTypeFlagsEXT             type,
                           const VkDebugUtilsMessengerCallbackDataEXT* data,
                           void*                                       userData);
} // namespace EVY_NAMESPACE

#endif