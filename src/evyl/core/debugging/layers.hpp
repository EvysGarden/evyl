#ifndef EVYL_CORE_DEBUGGING_LAYERS
#define EVYL_CORE_DEBUGGING_LAYERS

#include <evyl/core/tools/config.hpp>

#include <array>

namespace EVY_NAMESPACE {
    constexpr std::array<const char*, 1> VALIDATION_LAYERS { "VK_LAYER_KHRONOS_validation" };
}

#endif