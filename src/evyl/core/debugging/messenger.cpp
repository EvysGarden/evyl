#include "messenger.hpp"

#include <fmt/chrono.h>
#include <fmt/color.h>

namespace EVY_NAMESPACE {
    VkDebugUtilsMessengerCreateInfoEXT*
    populateDebugMessengerInfo(VkDebugUtilsMessengerCreateInfoEXT*                     info,
                               VkDebugUtilsMessageSeverityFlagsEXT                     severity,
                               const std::span<const VkDebugUtilsMessageTypeFlagsEXT>& types,
                               void*                                                   userData,
                               PFN_vkDebugUtilsMessengerCallbackEXT userCallback) {
        if (!info) return nullptr;

        info->sType           = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        info->pUserData       = userData;
        info->pfnUserCallback = userCallback;
        info->pNext           = nullptr;

        for (const auto& type : types) { info->messageType |= type; }

        switch (severity) {
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
                info->messageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT;

            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
                info->messageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT;

            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
                info->messageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;

            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
                info->messageSeverity |= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        }

        return info;
    }

    VKAPI_ATTR VkBool32 VKAPI_CALL
    debugMessengerCallback(VkDebugUtilsMessageSeverityFlagBitsEXT      severity,
                           VkDebugUtilsMessageTypeFlagsEXT             type,
                           const VkDebugUtilsMessengerCallbackDataEXT* data,
                           void*                                       userData) {
        assert(userData == nullptr);

        auto time = std::time(nullptr);
        switch (severity) {
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT: {
                fmt::print(fmt::fg(fmt::color::red) | fmt::emphasis::bold,
                           "{:%Y-%m-%d %H:%M:%S} [ERROR] {}\n",
                           fmt::localtime(time),
                           data->pMessage);
                break;
            }
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT: {
                fmt::print(fmt::fg(fmt::color::yellow) | fmt::emphasis::bold,
                           "{:%Y-%m-%d %H:%M:%S} [WARNING] {}\n",
                           fmt::localtime(time),
                           data->pMessage);
                break;
            }
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT: {
                fmt::print(
                    "{:%Y-%m-%d %H:%M:%S} [INFO] {}\n", fmt::localtime(time), data->pMessage);
                break;
            }
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT: {
                fmt::print(fmt::fg(fmt::color::gray) | fmt::emphasis::bold,
                           "{:%Y-%m-%d %H:%M:%S} [VERBOSE] {}\n",
                           fmt::localtime(time),
                           data->pMessage);
                break;
            }
            default: return VK_ERROR_FORMAT_NOT_SUPPORTED;
        }

        return VK_SUCCESS;
    }
} // namespace evy