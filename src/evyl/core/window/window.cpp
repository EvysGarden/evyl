#include "window.hpp"

namespace EVY_NAMESPACE {
    Window::Window(int width, int height, const std::string& title) {
        if (glfwInit() != GLFW_TRUE) {
            throw MonoException(make_error_code(eWindowResultGlfwError));
        }
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

        if (!(m_window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr))) {
            throw MonoException(make_error_code(eWindowResultError));
        }

        glfwSetWindowUserPointer(m_window, this);
    }

    void
    Window::setSize(int width, int height) noexcept {
        glfwSetWindowSize(m_window, width, height);
    }

    std::pair<int, int>
    Window::getSize() const noexcept {
        std::pair<int, int> size;
        glfwGetWindowSize(m_window, &size.first, &size.second);
        return size;
    }

    void
    Window::setTitle(const std::string& title) noexcept {
        glfwSetWindowTitle(m_window, title.c_str());
    }

    void
    Window::setVisible(bool visible) noexcept {
        if (visible) {
            glfwShowWindow(m_window);
        } else {
            glfwHideWindow(m_window);
        }
    }

    bool
    Window::isVisible() const noexcept {
        return glfwGetWindowAttrib(m_window, GLFW_VISIBLE);
    }

    bool
    Window::shouldClose() const noexcept {
        return glfwWindowShouldClose(m_window);
    }

    std::span<const char*>
    Window::getRequiredInstanceExtensions() noexcept {
        uint32_t size = 0;
        auto     raw  = glfwGetRequiredInstanceExtensions(&size);

        return { raw, size };
    }

    EVY_CREATE_ERROR_CATEGORY_CPP(Window) {
        switch (CODE) {
            case eWindowResultSuccess: return "No problems detected";
            case eWindowResultError: return "A general error has occured";
            case eWindowResultGlfwError: return "A glfw command failed";
            default: return "An unknown error has occured";
        }
    }
} // namespace EVY_NAMESPACE
