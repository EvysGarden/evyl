#ifndef EVYL_CORE_WINDOW_WINDOW
#define EVYL_CORE_WINDOW_WINDOW

#include <evyl/core/tools/common.hpp>
#include <GLFW/glfw3.h>
#include <span>
#include <string>

namespace EVY_NAMESPACE {
    class Window final : public StaticConstructible<Window> {
        private:
        GLFWwindow* m_window;

        public:
        /**
         * @brief Construct a new Window object
         *
         * @param width the width of the new window
         * @param height the height of the new window
         * @param title the title of the new window
         *
         * @throws MonoException if something goes wrong
         */
        Window(int width, int height, const std::string& title);

        /**
         * @brief A conversion function to get the underlying window
         *
         * @return GLFWwindow*
         */
        operator GLFWwindow*() const noexcept { return m_window; }

        /**
         * @brief Sets the size of the window
         */
        void
        setSize(int width, int height) noexcept;

        /**
         * @brief Get the size of the window
         *
         * @return std::pair<int, int> : pair of width and height
         */
        std::pair<int, int>
        getSize() const noexcept;

        /**
         * @brief Sets the title of the window
         */
        void
        setTitle(const std::string& title) noexcept;

        /**
         * @brief Sets the visibility of the window
         *
         * @param visible true for visible
         */
        void
        setVisible(bool visible) noexcept;

        /**
         * @brief Checks whether the window is visible or not
         *
         * @return true : window is visible
         * @return false : window is invisible
         */
        bool
        isVisible() const noexcept;

        /**
         * @brief returns the status of the window close flag
         *
         * @return true : window was asked to close
         * @return false : window was not asked to close
         */
        bool
        shouldClose() const noexcept;

        /**
         * @brief Get the Required Instance Extensions
         *
         * @return std::span<const char*> non-owning, GLFW will take care of deletion
         */
        static std::span<const char*>
        getRequiredInstanceExtensions() noexcept;
    };
    enum EVY_RESULT_ENUM_NAME(Window) : uint8_t
    {
        eWindowResultSuccess   = 0,
        eWindowResultError     = 1,
        eWindowResultGlfwError = 2
    };

    EVY_CREATE_ERROR_CATEGORY_HPP(Window);
} // namespace EVY_NAMESPACE

#endif