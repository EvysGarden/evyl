#ifndef EVYL_CORE_TOOLS_RESULT
#define EVYL_CORE_TOOLS_RESULT

#include <evyl/core/tools/config.hpp>

#include <functional>
#include <system_error>
#include <variant>

namespace EVY_NAMESPACE {
    template<typename T>
    class Result final {
        std::variant<T, std::error_code> m_value;

        public:
        Result(const T& value) noexcept : m_value(value) {}

        Result(T&& value) noexcept : m_value(std::move(value)) {}

        Result(const std::error_code& code) noexcept : m_value(code) {}

        Result(std::error_code&& code) noexcept : m_value(std::move(code)) {}

        Result(const Result&) noexcept = default;

        Result(Result&&) noexcept = default;

        Result&
        operator=(const Result&) noexcept = default;

        Result&
        operator=(Result&&) noexcept = default;

        Result&
        operator=(const std::error_code& code) noexcept {
            m_value = code;
        }

        Result&
        operator=(std::error_code&& code) noexcept {
            m_value = std::move(code);
        }

        Result&
        operator=(const T& value) noexcept {
            m_value = value;
        }

        Result&
        operator=(T&& value) noexcept {
            m_value = std::move(value);
        }

        bool
        has_value() const noexcept {
            return std::holds_alternative<T>(m_value);
        }

        operator bool() const noexcept { return std::holds_alternative<T>(m_value); }

        /**
         * @brief checks has_value() first. When true moves internal value to parameter value.
         * Otherwise executes the provieded function with the error code.
         *
         * @param value destination value to move the internal value to
         * @param fun function to call on failure.
         * @return true moving was successful 
         * @return false moving wasn't successful, called fun
         */
        bool
        value_if(T& value, std::function<void(std::error_code& code)> fun) {
            if (has_value()) {
                value = std::move(std::get<T>(m_value));
                return true;
            }
            fun(std::get<std::error_code>(m_value));
            return false;
        }

        T&
        value() & {
            return std::get<T>(m_value);
        }

        T&&
        value() && {
            if (!has_value()) return nullptr;
            return std::move(std::get<T>(m_value));
        }

        const T&
        value() const& {
            if (!has_value()) return nullptr;
            return std::get<T>(m_value);
        }

        const T&&
        value() const&& {
            if (!has_value()) return nullptr;
            return std::move(std::get<T>(m_value));
        }

        std::error_code
        error() const {
            return std::get<std::error_code>(m_value);
        }

        T&
        operator*() & {
            if (!has_value()) return nullptr;
            return std::get<T>(m_value);
        }

        const T&
        operator*() const& {
            if (!has_value()) return nullptr;
            return std::get<T>(m_value);
        }

        T&&
        operator*() && {
            if (!has_value()) return nullptr;
            return std::move(std::get<T>(m_value));
        }

        T*
        operator->() {
            if (!has_value()) return nullptr;
            return &std::get<T>(m_value);
        }

        const T*
        operator->() const {
            if (!has_value()) return nullptr;
            return &std::get<T>(m_value);
        }
    };
} // namespace EVY_NAMESPACE
#endif