#ifndef EVYL_CORE_TOOLS_TRAITS
#define EVYL_CORE_TOOLS_TRAITS

#include "exceptions.hpp"
#include "result.hpp"

#include <system_error>
#include <variant>

namespace EVY_NAMESPACE {
    template<class _Base>
    class StaticConstructible {
        public:
        template<typename... _Args>
        static Result<_Base*>
        create(_Args&&... args) noexcept {
            try {
                return new _Base { std::forward<_Args>(args)... };
            } catch (const MonoException<std::error_code>& e) { return Result<_Base*>{e}; }
        }
    };
} // namespace evy

#endif