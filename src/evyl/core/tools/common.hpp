#ifndef EVYL_CORE_TOOLS_COMMON
#define EVYL_CORE_TOOLS_COMMON

#include "exceptions.hpp"
#include "macros.hpp"
#include "needs.hpp"
#include "result.hpp"
#include "traits.hpp"
#include "types.hpp"
#include "vkresult.hpp"

#endif