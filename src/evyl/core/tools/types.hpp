#ifndef EVYL_CORE_TOOLS_TYPES
#define EVYL_CORE_TOOLS_TYPES

#include "config.hpp"

#include <memory>

namespace EVY_NAMESPACE {
    /**
     * @brief some useful type defines
     */
    template<typename T>
    using unique = std::unique_ptr<T>;
    template<typename T>
    using shared = std::shared_ptr<T>;

} // namespace EVY_NAMESPACE
#endif