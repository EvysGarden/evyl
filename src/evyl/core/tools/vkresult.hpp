#ifndef EVYL_CORE_TOOLS_VKRESULT
#define EVYL_CORE_TOOLS_VKRESULT

#include <evyl/core/tools/config.hpp>
#include <string>
#include <system_error>
#include <vulkan/vulkan_core.h>

namespace EVY_NAMESPACE {
    inline class VkResultCategory final : public std::error_category {
        const char*
        name() const noexcept override;
        std::string
        message(int) const noexcept override;
    } g_VkResultCategory {};

    VkResultCategory&
    VkResultCategory() noexcept;

    std::error_code
    make_error_code(VkResult code) noexcept;
} // namespace EVY_NAMESPACE

#endif