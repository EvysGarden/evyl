#ifndef EVYL_CORE_TOOLS_EXCEPTIONS
#define EVYL_CORE_TOOLS_EXCEPTIONS

#include "config.hpp"

#include <exception>
#include <fmt/format.h>
#include <system_error>
#include <type_traits>

namespace EVY_NAMESPACE {
    template<std::copy_constructible T>
    class MonoException final : public std::exception {
        const T resultCode;

        public:
        explicit MonoException(const T& resultCode) : resultCode(resultCode) {}

        inline const char*
        what() const noexcept override {
            std::string res;
            if constexpr (std::is_same_v<T, std::error_code>) {
                const std::error_code& err = resultCode;
                res = fmt::format("MonoException thrown with error code {} from {}: {}",
                                  err.value(),
                                  err.category().name(),
                                  err.message());
            } else if constexpr (std::is_convertible_v<T, size_t>) {
                res = fmt::format("MonoException thrown with {}", resultCode);
            } else {
                return nullptr;
            }
            return strcpy(new char[res.size()], res.c_str());
        }

        operator T() const { return resultCode; }
    };
} // namespace evy

#endif