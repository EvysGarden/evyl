#ifndef EVYL_CORE_TOOLS_MACROS
#define EVYL_CORE_TOOLS_MACROS

/**
 * @brief Some macro tools
 */
#define CONCAT2(x, y) x##y
#define CONCAT(x, y)  CONCAT2(x, y)
#define STRINGIGY2(x) #x
#define STRINGIGY(x)  STRINGIGY2(x)

/**
 * @brief Use these to comply with naming conventions in other macros.
 *
 * @example :
 * EVY_RESULT_ENUM_NAME(MyClass) evaluates to MyClassResultEnum
 * EVY_RESULT_CATEGORY_NAME(MyClass) evaluates to MyClassResultCategory
 */
#define EVY_RESULT_ENUM_NAME(m_name)     CONCAT(m_name, ResultEnum)
#define EVY_RESULT_CATEGORY_NAME(m_name) CONCAT(m_name, ResultCategory)

/**
 * @brief This Macro automatically creates error category support for a class.
 * It provides <name>ResultCategory and requires <name>ResultEnum.
 * Use this in your cpp file together with the HPP counterpart in an included header.
 *
 * @param m_name : the base name. See description above.
 *
 * You will need to return the message c-strings for your enums.
 * The enum is given as an int named CODE
 *
 * @example :
    EVY_CREATE_ERROR_CATEGORY_CPP(Window) {
        switch (CODE) {
            case eWindowResultSuccess: return "No problems detected";
            case eWindowResultError: return "A general error has occured";
            case eWindowResultGlfwError: return "A glfw command failed";
            default: return "An unknown error has occured";
        }
    }
 */
#define EVY_CREATE_ERROR_CATEGORY_CPP(m_name)                                              \
    const char* EVY_RESULT_CATEGORY_NAME(m_name)::name() const noexcept {                  \
        return STRINGIGY(EVY_RESULT_ENUM_NAME(m_name));                                    \
    }                                                                                      \
                                                                                           \
    class EVY_RESULT_CATEGORY_NAME(m_name) & EVY_RESULT_CATEGORY_NAME(m_name)() noexcept { \
        return CONCAT(g_, EVY_RESULT_CATEGORY_NAME(m_name));                               \
    }                                                                                      \
                                                                                           \
    std::error_code make_error_code(EVY_RESULT_ENUM_NAME(m_name) code) noexcept {          \
        return std::error_code(code, EVY_RESULT_CATEGORY_NAME(m_name)());                  \
    }                                                                                      \
                                                                                           \
    std::string EVY_RESULT_CATEGORY_NAME(m_name)::message(int CODE) const noexcept

/**
 * @brief This Macro automatically creates error category support for a class.
 * It provides <name>ResultCategory and requires <name>ResultEnum.
 * Use this in your hpp file.
 *
 * @param m_name : the base name. See description above.
 *
 *
 * @example :
    enum EVY_RESULT_ENUM_NAME(Window) : uint8_t
    {
        eWindowResultSuccess   = 0,
        eWindowResultError     = 1,
        eWindowResultGlfwError = 2
    };

    EVY_CREATE_ERROR_CATEGORY_HPP(Window);
 */
#define EVY_CREATE_ERROR_CATEGORY_HPP(m_name)                                          \
    inline class EVY_RESULT_CATEGORY_NAME(m_name) final : public std::error_category { \
        const char*                                                                    \
        name() const noexcept override;                                                \
        std::string                                                                    \
        message(int) const noexcept override;                                          \
    } CONCAT(g_, EVY_RESULT_CATEGORY_NAME(m_name)) {};                                 \
                                                                                       \
    EVY_RESULT_CATEGORY_NAME(m_name)                                                   \
    &EVY_RESULT_CATEGORY_NAME(m_name)() noexcept;                                      \
                                                                                       \
    std::error_code make_error_code(EVY_RESULT_ENUM_NAME(m_name) code) noexcept;

#endif