#ifndef EVYL_CORE_TOOLS_NEEDS
#define EVYL_CORE_TOOLS_NEEDS

#include "config.hpp"
#include "types.hpp"

namespace EVY_NAMESPACE {
    /**
     * @brief The needs class provides a way to store shared
     * pointers to ensure the correct order of
     * destruction.
     *
     * @tparam T the class to store the shared pointer of
     */
    template<typename T>
    class Needs {
        protected:
        shared<T> res;

        Needs(const shared<T> res) : res(res) {}

        shared<T>
        ptr() noexcept {
            return res;
        }
        T&
        ref() const {
            return *res;
        }
    };
} // namespace EVY_NAMESPACE

#endif