#ifndef EVYL_CORE_INSTANCE_FACTORY
#define EVYL_CORE_INSTANCE_FACTORY

#include "features.hpp"
#include "instance.hpp"

#include <evyl/core/tools/common.hpp>

namespace EVY_NAMESPACE {
    class InstanceFactory final {
        VkApplicationInfo                   m_appInfo;
        void*                               m_pNext;
        VkInstanceCreateFlags               m_flags;
        std::vector<const char*>            m_extensions;
        std::vector<const char*>            m_layers;
        VkDebugUtilsMessengerCreateInfoEXT* m_messengerInfo;
        unique<Features>                    m_features;

        public:
        InstanceFactory();
        ~InstanceFactory();

        Result<Instance*>
        build() const noexcept;

        /**
         * @brief builds and returns an instance. Deletes itself. (Don't use this factory afterwards
         * and allocate with new!)
         * 
         * @return Result<Instance*> 
         */
        Result<Instance*>
        buildAndDestruct() const noexcept;

        InstanceFactory&
        setApiVersion(uint32_t version) noexcept;

        InstanceFactory&
        setAppName(const std::string& name) noexcept;

        InstanceFactory&
        setAppVersion(std::span<const int, 4> version) noexcept;

        InstanceFactory&
        setAppVersion(const std::array<int, 4>& version) noexcept;

        InstanceFactory&
        setEngineName(const std::string& name) noexcept;

        InstanceFactory&
        setEngineVersion(std::span<const int, 4> version) noexcept;

        InstanceFactory&
        setEngineVersion(const std::array<int, 4>& version) noexcept;

        InstanceFactory&
        addExtension(const char* const extension) noexcept;

        InstanceFactory&
        addExtensions(const std::span<const char* const>& extensions) noexcept;

        InstanceFactory&
        addExtensions(std::initializer_list<const char* const> extensions) noexcept;

        InstanceFactory&
        addLayer(const char* const layer) noexcept;

        InstanceFactory&
        addLayers(const std::span<const char* const>& layers) noexcept;

        InstanceFactory&
        addLayers(std::initializer_list<const char* const> layers) noexcept;

        InstanceFactory&
        enableDebugging(
            VkDebugUtilsMessageSeverityFlagsEXT                          severity,
            std::initializer_list<const VkDebugUtilsMessageTypeFlagsEXT> types) noexcept;

        InstanceFactory&
        enableDebugging(VkDebugUtilsMessageSeverityFlagsEXT                     severity,
                        const std::span<const VkDebugUtilsMessageTypeFlagsEXT>& types) noexcept;

        InstanceFactory&
        disableDebugging() noexcept;

        InstanceFactory&
        setCustomDebugCallback(PFN_vkDebugUtilsMessengerCallbackEXT userCallback,
                               void*                                userData) noexcept;

        operator VkInstanceCreateInfo() const noexcept;
    };
} // namespace EVY_NAMESPACE

#endif