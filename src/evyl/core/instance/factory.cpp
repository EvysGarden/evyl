#include "factory.hpp"

#include <evyl/core/debugging/layers.hpp>
#include <evyl/core/debugging/messenger.hpp>

namespace EVY_NAMESPACE {
    InstanceFactory::InstanceFactory() {
        m_appInfo.sType            = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        m_appInfo.pApplicationName = nullptr;
        m_appInfo.pEngineName      = nullptr;
        m_appInfo.pNext            = nullptr;
        m_appInfo.apiVersion       = VK_API_VERSION_1_3;
        m_pNext                    = nullptr;
        m_messengerInfo            = nullptr;

        auto features = Features::create();
        if (!features.has_value()) throw MonoException(features.error());
        m_features.reset(features.value());
    }

    InstanceFactory::~InstanceFactory() {
        delete[] m_appInfo.pApplicationName;
        delete[] m_appInfo.pEngineName;
        delete m_messengerInfo;
    }

    Result<Instance*>
    InstanceFactory::build() const noexcept {
        if (!m_features->areExtensionsAvailable(m_extensions)) {
            return make_error_code(eInstanceResultExtensionNotPresent);
        }
        if (!m_features->areLayersAvailable(m_layers)) {
            return make_error_code(eInstanceResultLayerNotPresent);
        }
        VkInstanceCreateInfo info = *this;
        info.pNext = m_messengerInfo;
        return Instance::create(info);
    }

    Result<Instance*>
    InstanceFactory::buildAndDestruct() const noexcept {
        auto result = build();
        delete this;
        return result;
    }

    InstanceFactory&
    InstanceFactory::setApiVersion(uint32_t version) noexcept {
        m_appInfo.apiVersion = version;
        return *this;
    }

    InstanceFactory&
    InstanceFactory::setAppName(const std::string& name) noexcept {
        m_appInfo.pApplicationName = strcpy(new char[name.size()], name.c_str());
        return *this;
    }

    InstanceFactory&
    InstanceFactory::setAppVersion(std::span<const int, 4> version) noexcept {
        m_appInfo.applicationVersion
            = VK_MAKE_API_VERSION(version[0], version[1], version[2], version[3]);
        return *this;
    }

    InstanceFactory&
    InstanceFactory::setAppVersion(const std::array<int, 4>& version) noexcept {
        setAppVersion(std::span(version));
        return *this;
    }

    InstanceFactory&
    InstanceFactory::setEngineName(const std::string& name) noexcept {
        m_appInfo.pEngineName = strcpy(new char[name.size()], name.c_str());
        return *this;
    }

    InstanceFactory&
    InstanceFactory::setEngineVersion(std::span<const int, 4> version) noexcept {
        m_appInfo.engineVersion
            = VK_MAKE_API_VERSION(version[0], version[1], version[2], version[3]);
        return *this;
    }

    InstanceFactory&
    InstanceFactory::setEngineVersion(const std::array<int, 4>& version) noexcept {
        setEngineVersion(std::span(version));
        return *this;
    }

    InstanceFactory&
    InstanceFactory::addExtension(const char* const extension) noexcept {
        m_extensions.push_back(extension);
        return *this;
    }

    InstanceFactory&
    InstanceFactory::addExtensions(const std::span<const char* const>& extensions) noexcept {
        m_extensions.reserve(extensions.size());
        for (const auto& extension : extensions) { this->addExtension(extension); }
        return *this;
    }

    InstanceFactory&
    InstanceFactory::addExtensions(std::initializer_list<const char* const> extensions) noexcept {
        return this->addExtensions(std::span(extensions));
    }

    InstanceFactory&
    InstanceFactory::addLayer(const char* const layer) noexcept {
        m_layers.push_back(layer);
        return *this;
    }

    InstanceFactory&
    InstanceFactory::addLayers(const std::span<const char* const>& layers) noexcept {
        m_layers.reserve(layers.size());
        for (const auto& layer : layers) { m_layers.push_back(layer); }
        return *this;
    }

    InstanceFactory&
    InstanceFactory::addLayers(std::initializer_list<const char* const> layers) noexcept {
        return this->addLayers(std::span(layers));
    }

    InstanceFactory&
    InstanceFactory::enableDebugging(
        VkDebugUtilsMessageSeverityFlagsEXT                          severity,
        std::initializer_list<const VkDebugUtilsMessageTypeFlagsEXT> types) noexcept {
        return this->enableDebugging(severity, std::span(types));
    }

    InstanceFactory&
    InstanceFactory::disableDebugging() noexcept {
        delete m_messengerInfo;
        return *this;
    }

    InstanceFactory&
    InstanceFactory::enableDebugging(
        VkDebugUtilsMessageSeverityFlagsEXT                     severity,
        const std::span<const VkDebugUtilsMessageTypeFlagsEXT>& types) noexcept {
        delete m_messengerInfo;
        m_messengerInfo = populateDebugMessengerInfo(new VkDebugUtilsMessengerCreateInfoEXT(),
                                                     severity,
                                                     types,
                                                     nullptr,
                                                     debugMessengerCallback);
        this->addLayers(VALIDATION_LAYERS);
        return *this;
    }

    InstanceFactory&
    InstanceFactory::setCustomDebugCallback(PFN_vkDebugUtilsMessengerCallbackEXT userCallback,
                                            void* userData) noexcept {
        if (m_messengerInfo) {
            m_messengerInfo->pUserData       = userData;
            m_messengerInfo->pfnUserCallback = userCallback;
        }
        return *this;
    }

    InstanceFactory::operator VkInstanceCreateInfo() const noexcept {
        VkInstanceCreateInfo info;
        info.sType                   = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        info.pApplicationInfo        = &m_appInfo;
        info.pNext                   = m_pNext;
        info.enabledExtensionCount   = m_extensions.size();
        info.ppEnabledExtensionNames = m_extensions.data();
        info.enabledLayerCount       = m_layers.size();
        info.ppEnabledLayerNames     = m_layers.data();
        info.flags                   = 0;

        return info;
    }
} // namespace evy