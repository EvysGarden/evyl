#ifndef EVYL_CORE_INSTANCE_INSTANCE
#define EVYL_CORE_INSTANCE_INSTANCE


#include <evyl/core/tools/common.hpp>

#include <vulkan/vulkan_core.h>

#if !defined(EVYL_INSTANCE_NO_INTERRUPT)
#include <csignal>

namespace {
    inline volatile std::sig_atomic_t g_SignalStatus;
}
#endif

namespace EVY_NAMESPACE {
    class InstanceFactory;

    class Instance final : public StaticConstructible<Instance> {
        VkInstance m_Instance;

        public:
        /**
         * @brief Construct a new Instance object
         *
         * @param info the instance create info
         * @throws MonoException if something goes wrong
         */
        explicit Instance(const VkInstanceCreateInfo& info) {
            if (auto result = vkCreateInstance(&info, nullptr, &m_Instance)) {
                throw MonoException(make_error_code(result));
            }

#if !defined(EVYL_INSTANCE_NO_INTERRUPT)
            std::signal(SIGINT, signalHandler);
#endif
        }

        /**
         * @brief helper function to query for all available physical devices for this instance
         *
         * @return std::vector<VkPhysicalDevice>
         */
        std::vector<VkPhysicalDevice>
        queryPhysicalDevices() const;

        static InstanceFactory* createFactory();

        /**
         * @brief conversion function to get the underlying VkInstance
         *
         * @return VkInstance
         */
        inline operator VkInstance() const noexcept { return m_Instance; }

#if !defined(EVYL_INSTANCE_NO_INTERRUPT)
        /**
         * @brief checks whether a signal was recieved
         *
         * @return true : signal recieved
         * @return false : signal not recieved
         */
        static bool
        interrupted();

        private:
        /**
         * @brief static signal callback which sets g_SignalStatus
         * to true in case a signal was recieved.
         *
         * @param signal the signal to watch
         */
        static void
        signalHandler(int signal);
#endif
    };
} // namespace EVY_NAMESPACE

#endif