#ifndef EVYL_CORE_INSTANCE_RESULT
#define EVYL_CORE_INSTANCE_RESULT

#include <evyl/core/tools/common.hpp>

namespace EVY_NAMESPACE {
    enum EVY_RESULT_ENUM_NAME(Instance) : uint8_t
    {
        eInstanceResultSuccess             = 0,
        eInstanceResultError               = 1,
        eInstanceResultLayersFailed        = 2,
        eInstanceResultExtensionsFailed    = 3,
        eInstanceResultLayerNotPresent     = 4,
        eInstanceResultExtensionNotPresent = 5,
        eInstanceResultNoDevicesFound      = 6
    };

    EVY_CREATE_ERROR_CATEGORY_HPP(Instance);
} // namespace EVY_NAMESPACE

#endif