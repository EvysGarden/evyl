#include "result.hpp"

namespace EVY_NAMESPACE {
    EVY_CREATE_ERROR_CATEGORY_CPP(Instance) {
        switch (CODE) {
            case eInstanceResultSuccess: return "No problems detected";
            case eInstanceResultError: return "A general error has occured";
            case eInstanceResultLayersFailed: return "Layer enumeration failed";
            case eInstanceResultLayerNotPresent: return "A requested layer was not found";
            case eInstanceResultExtensionNotPresent: return "A requested extension wasn't found";
            case eInstanceResultNoDevicesFound: return "No devices were found";
            default: return "An unknown error has occured";
        }
    }
} // namespace EVY_NAMESPACE