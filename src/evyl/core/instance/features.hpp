#ifndef EVYL_CORE_INSTANCE_FEATURES
#define EVYL_CORE_INSTANCE_FEATURES

#include "result.hpp"

#include <evyl/core/tools/common.hpp>

#include <bits/ranges_util.h>
#include <set>
#include <span>

namespace EVY_NAMESPACE {
    /**
     * @brief a little helper class which gets teh available layers and extensions and provides
     * simple check functions for later use.
     *
     */
    class Features final : public StaticConstructible<Features> {
        unique<std::vector<VkLayerProperties>>     m_layers;
        unique<std::vector<VkExtensionProperties>> m_extensions;

        public:
        /**
         * @brief Construct a new Features object.
         * queries layers and extensions.
         */
        Features();

        private:
        /**
         * @brief queries the available layers
         *
         * @return EVY_RESULT_ENUM_NAME(Instance) (0 on success)
         */
        EVY_RESULT_ENUM_NAME(Instance) queryLayers();

        /**
         * @brief queries the available extensions
         *
         * @return EVY_RESULT_ENUM_NAME(Instance) (0 on success)
         */
        EVY_RESULT_ENUM_NAME(Instance) queryExtensions();

        public:
        /**
         * @brief checks whether the layer is available
         *
         * @param layer the layer to search for
         * @return true : the layer was found and is available for use
         * @return false : the layer was not found and is not available for use
         */
        bool
        isLayerAvailable(const std::string& layer) const noexcept;

        /**
         * @brief checks whether the layers are available
         *
         * @param layers the layers to search for
         * @return true : the layers were found and are available for use
         * @return false : the layer were not found and are not available for use
         */
        bool
        areLayersAvailable(const std::span<const char* const>& layers) const noexcept;

        /**
         * @brief checks whether the layers are available
         *
         * @param layers the layers to search for
         * @return true : the layers were found and are available for use
         * @return false : the layer were not found and are not available for use
         */
        bool
        areLayersAvailable(const std::span<std::string>& layers) const noexcept;

        /**
         * @brief checks whether the extension is available
         *
         * @param extension the extension to search for
         * @return true : the extension was found and is available for use
         * @return false : the extension was not found and is not available for use
         */
        bool
        isExtensionAvailable(const std::string& extension) const noexcept;

        /**
         * @brief checks whether the extensions are available
         *
         * @param extensions the extensions to search for
         * @return true : the extensions were found and are available for use
         * @return false : the layer were not found and are not available for use
         */
        bool
        areExtensionsAvailable(const std::span<const char* const>& extensions) const noexcept;

        /**
         * @brief checks whether the extensions are available
         *
         * @param extensions the extensions to search for
         * @return true : the extensions were found and are available for use
         * @return false : the layer were not found and are not available for use
         */
        bool
        areExtensionsAvailable(const std::span<std::string>& extensions) const noexcept;
    };
} // namespace EVY_NAMESPACE

#endif