#include "instance.hpp"
#include "factory.hpp"

namespace EVY_NAMESPACE {
    std::vector<VkPhysicalDevice>
    Instance::queryPhysicalDevices() const {
        uint32_t count = 0;
        if (auto result = vkEnumeratePhysicalDevices(m_Instance, &count, nullptr)) {
            throw MonoException(make_error_code(result));
        }

        if (!count) { throw MonoException(make_error_code(eInstanceResultNoDevicesFound)); }

        std::vector<VkPhysicalDevice> devices(count);
        vkEnumeratePhysicalDevices(m_Instance, &count, devices.data());

        return devices;
    }

    InstanceFactory*
    Instance::createFactory() {
        return new InstanceFactory();
    }

#if !defined(EVYL_INSTANCE_NO_INTERRUPT)
    bool
    Instance::interrupted() {
        return g_SignalStatus == SIGINT;
    }

    void
    Instance::signalHandler(int signal) {
        g_SignalStatus = signal;
    }
#endif
} // namespace EVY_NAMESPACE