#include "features.hpp"

namespace EVY_NAMESPACE {

    EVY_RESULT_ENUM_NAME(Instance) Features::queryLayers() {
        uint32_t count = 0;
        if (vkEnumerateInstanceLayerProperties(&count, nullptr) != VK_SUCCESS) {
            return eInstanceResultLayersFailed;
        }

        m_layers = std::make_unique<std::vector<VkLayerProperties>>(count);
        vkEnumerateInstanceLayerProperties(&count, m_layers->data());

        return eInstanceResultSuccess;
    }

    EVY_RESULT_ENUM_NAME(Instance) Features::queryExtensions() {
        uint32_t count = 0;
        if (vkEnumerateInstanceExtensionProperties(nullptr, &count, nullptr) != VK_SUCCESS) {
            return eInstanceResultExtensionsFailed;
        }

        m_extensions = std::make_unique<std::vector<VkExtensionProperties>>(count);
        vkEnumerateInstanceExtensionProperties(nullptr, &count, m_extensions->data());

        return eInstanceResultSuccess;
    }

    bool
    Features::isLayerAvailable(const std::string& layer) const noexcept {
        if (!m_layers) return false;

        auto containsLayer = [&](auto l) { return std::string(l.layerName) == layer; };

        return std::ranges::find_if(*m_layers, containsLayer) != m_layers->end();
    }

    bool
    Features::areLayersAvailable(const std::span<const char* const>& layers) const noexcept {
        if (!m_layers) return false;
        if (layers.empty()) return true;

        std::set<std::string> layersSet(layers.begin(), layers.end());
        for (const auto& layer : *m_layers) {
            layersSet.erase(std::string(layer.layerName));
            if (layersSet.empty()) return true;
        }

        return layersSet.empty();
    }

    bool
    Features::areLayersAvailable(const std::span<std::string>& layers) const noexcept {
        if (!m_layers) return false;
        if (layers.empty()) return true;

        std::set<std::string> layersSet(layers.begin(), layers.end());
        for (const auto& layer : *m_layers) {
            layersSet.erase(std::string(layer.layerName));
            if (layersSet.empty()) return true;
        }

        return layersSet.empty();
    }

    bool
    Features::isExtensionAvailable(const std::string& extension) const noexcept {
        if (m_extensions->empty()) return false;

        auto containsExtension = [&](auto e) { return std::string(e.extensionName) == extension; };

        return std::ranges::find_if(*m_extensions, containsExtension) != m_extensions->end();
    }

    bool
    Features::areExtensionsAvailable(
        const std::span<const char* const>& extensions) const noexcept {
        if (!m_extensions) return false;
        if (extensions.empty()) return true;

        std::set<std::string> extSet(extensions.begin(), extensions.end());
        for (const auto& ext : *m_extensions) {
            extSet.erase(std::string(ext.extensionName));
            if (extSet.empty()) return true;
        }

        return extSet.empty();
    }

    bool
    Features::areExtensionsAvailable(const std::span<std::string>& extensions) const noexcept {
        if (!m_extensions) return false;
        if (extensions.empty()) return true;

        std::set<std::string> extSet(extensions.begin(), extensions.end());
        for (const auto& ext : *m_extensions) {
            extSet.erase(std::string(ext.extensionName));
            if (extSet.empty()) return true;
        }

        return extSet.empty();
    }

    Features::Features() {
        if (auto result = queryLayers()) { throw MonoException(make_error_code(result)); }
        if (auto result = queryExtensions()) { throw MonoException(make_error_code(result)); }
    }
} // namespace EVY_NAMESPACE