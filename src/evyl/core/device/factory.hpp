#ifndef EVYL_CORE_DEVICE_FACTORY
#define EVYL_CORE_DEVICE_FACTORY

#include "device.hpp"

#include <evyl/core/tools/common.hpp>

namespace EVY_NAMESPACE {
    class DeviceFactory final {
        VkDeviceCreateInfo               m_deviceInfo;
        std::vector<const char*>         m_extensions;
        std::vector<const char*>         m_layers;
        unique<VkPhysicalDeviceFeatures> m_features;

        public:
        DeviceFactory() noexcept;

        Result<Device*>
        build(const shared<Surface>& surface) const noexcept;

        /**
         * @brief builds and returns a device. Deletes itself. (Don't use this factory afterwards
         * and allocate with new!)
         *
         * @param surface
         * @return Result<Device*>
         */
        Result<Device*>
        buildAndDestruct(const shared<Surface>& surface) const noexcept;

        operator VkDeviceCreateInfo() const noexcept;

        DeviceFactory&
        addExtension(const char* const extension) noexcept;

        DeviceFactory&
        addExtensions(const std::span<const char* const>& extensions) noexcept;

        DeviceFactory&
        addExtensions(std::initializer_list<const char* const> extensions) noexcept;

        DeviceFactory&
        addLayer(const char* const layer) noexcept;

        DeviceFactory&
        addLayers(const std::span<const char* const>& layers) noexcept;

        DeviceFactory&
        addLayers(std::initializer_list<const char* const> layers) noexcept;

        DeviceFactory&
        addValidationLayers() noexcept;

        DeviceFactory&
        setDeviceFeatures(const std::function<void(VkPhysicalDeviceFeatures* const)>& fun) noexcept;
    };

} // namespace EVY_NAMESPACE

#endif