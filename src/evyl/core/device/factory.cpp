#include "factory.hpp"

#include <evyl/core/debugging/layers.hpp>

namespace EVY_NAMESPACE {
    DeviceFactory::DeviceFactory() noexcept {
        m_deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        m_deviceInfo.flags = 0;
    }

    Result<Device*>
    DeviceFactory::build(const shared<Surface>& surface) const noexcept {
        return Device::create(surface, *this);
    }

    Result<Device*>
    DeviceFactory::buildAndDestruct(const shared<Surface>& surface) const noexcept {
        auto result = Device::create(surface, *this);
        delete this;
        return result;
    }

    DeviceFactory::operator VkDeviceCreateInfo() const noexcept {
        VkDeviceCreateInfo info      = m_deviceInfo;
        info.enabledExtensionCount   = m_extensions.size();
        info.ppEnabledExtensionNames = m_extensions.data();
        info.enabledLayerCount       = m_layers.size();
        info.ppEnabledLayerNames     = m_extensions.data();
        info.pEnabledFeatures        = m_features ? m_features.get() : nullptr;

        return info;
    }

    DeviceFactory&
    DeviceFactory::addExtension(const char* const extension) noexcept {
        m_extensions.push_back(extension);
        return *this;
    }

    DeviceFactory&
    DeviceFactory::addExtensions(const std::span<const char* const>& extensions) noexcept {
        m_extensions.reserve(extensions.size());
        for (const auto& extension : extensions) { this->addExtension(extension); }
        return *this;
    }

    DeviceFactory&
    DeviceFactory::addExtensions(std::initializer_list<const char* const> extensions) noexcept {
        return this->addExtensions(std::span(extensions));
    }

    DeviceFactory&
    DeviceFactory::addLayer(const char* const layer) noexcept {
        m_layers.push_back(layer);
        return *this;
    }

    DeviceFactory&
    DeviceFactory::addLayers(const std::span<const char* const>& layers) noexcept {
        m_layers.reserve(layers.size());
        for (const auto& layer : layers) { m_layers.push_back(layer); }
        return *this;
    }

    DeviceFactory&
    DeviceFactory::addLayers(std::initializer_list<const char* const> layers) noexcept {
        return this->addLayers(std::span(layers));
    }

    DeviceFactory&
    DeviceFactory::addValidationLayers() noexcept {
        return this->addLayers(VALIDATION_LAYERS);
    }

    DeviceFactory&
    DeviceFactory::setDeviceFeatures(
        const std::function<void(VkPhysicalDeviceFeatures* const)>& fun) noexcept {
        if (!m_features) { m_features = std::make_unique<VkPhysicalDeviceFeatures>(); }

        fun(m_features.get());

        return *this;
    }
} // namespace EVY_NAMESPACE