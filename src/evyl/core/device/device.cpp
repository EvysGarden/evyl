#include "device.hpp"
#include "factory.hpp"

namespace EVY_NAMESPACE {
    Device::Device(const shared<Surface>& surface, VkDeviceCreateInfo info) :
        Needs<Surface>(surface) {
        // pick a device and get its queue family indices
        auto physicalDeviceResult = surface->pickPhysicalDevice(
            std::span(info.ppEnabledExtensionNames, info.enabledExtensionCount));

        if (!physicalDeviceResult) { throw MonoException(physicalDeviceResult.error()); }
        m_physical = physicalDeviceResult.value().first;

        // now for the queue families
        const auto& familyIndices = physicalDeviceResult.value().second;

        std::set<uint32_t> uniqueFamilyIndices = { familyIndices.compute.value(),
                                                   familyIndices.graphics.value(),
                                                   familyIndices.present.value(),
                                                   familyIndices.transfer.value() };

        // since we use only one queue per family, they all get the same priority
        float                                stableQueuePriority = 1.0F;
        std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
        queueCreateInfos.reserve(uniqueFamilyIndices.size());
        for (auto index : uniqueFamilyIndices) {
            VkDeviceQueueCreateInfo queueCreateInfo {};
            queueCreateInfo.sType            = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queueCreateInfo.queueFamilyIndex = index;
            queueCreateInfo.queueCount       = 1;
            queueCreateInfo.pQueuePriorities = &stableQueuePriority;
            queueCreateInfo.pNext            = nullptr;
            queueCreateInfo.flags            = 0;
            queueCreateInfos.push_back(queueCreateInfo);
        }

        // adding the queues family indices to the info and creating the device
        info.queueCreateInfoCount = queueCreateInfos.size();
        info.pQueueCreateInfos    = queueCreateInfos.data();
        if (auto result = vkCreateDevice(m_physical, &info, nullptr, &m_logical)) {
            throw MonoException(make_error_code(result));
        }

        // now to retrieve the queues from the logical device
        VkQueue computeQueue;
        vkGetDeviceQueue(m_logical, familyIndices.compute.value(), 0, &computeQueue);
        m_queues.compute = { computeQueue, familyIndices.compute.value() };

        VkQueue graphicsQueue;
        vkGetDeviceQueue(m_logical, familyIndices.graphics.value(), 0, &graphicsQueue);
        m_queues.graphics = { graphicsQueue, familyIndices.graphics.value() };

        VkQueue presentQueue;
        vkGetDeviceQueue(m_logical, familyIndices.present.value(), 0, &presentQueue);
        m_queues.present = { presentQueue, familyIndices.present.value() };

        VkQueue transferQueue;
        vkGetDeviceQueue(m_logical, familyIndices.transfer.value(), 0, &transferQueue);
        m_queues.transfer = { transferQueue, familyIndices.transfer.value() };
    }


    DeviceFactory* Device::createFactory() noexcept {
        return new DeviceFactory();
    }
} // namespace EVY_NAMESPACE