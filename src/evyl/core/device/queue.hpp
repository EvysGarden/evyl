#ifndef EVYL_CORE_DEVICE_QUEUE
#define EVYL_CORE_DEVICE_QUEUE

#include <evyl/core/tools/config.hpp>

#include <optional>
#include <vulkan/vulkan_core.h>

namespace EVY_NAMESPACE {
    struct Queue final {
        VkQueue  m_queue;
        uint32_t m_index;

        Queue() : m_queue(nullptr), m_index() {}
        Queue(VkQueue queue, uint32_t index) : m_queue(queue), m_index(index) {}

        inline operator VkQueue() const noexcept { return m_queue; }
        inline operator std::optional<uint32_t>() const noexcept {
            return m_queue ? std::optional<uint32_t>(m_index) : std::nullopt;
        }
    };
} // namespace EVY_NAMESPACE

#endif