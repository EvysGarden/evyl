#ifndef EVYL_CORE_DEVICE_DEVICE
#define EVYL_CORE_DEVICE_DEVICE

#include "queue.hpp"
#include "result.hpp"

#include <evyl/core/surface/surface.hpp>

namespace EVY_NAMESPACE {
    class DeviceFactory;
    class Device final : public StaticConstructible<Device>, protected Needs<Surface> {
        VkDevice         m_logical;
        VkPhysicalDevice m_physical;

        struct final {
            Queue graphics;
            Queue compute;
            Queue transfer;
            Queue present;
        } m_queues;

        public:
        Device(const shared<Surface>& surface, VkDeviceCreateInfo info);

        static DeviceFactory*
        createFactory() noexcept;
    };
} // namespace EVY_NAMESPACE

#endif