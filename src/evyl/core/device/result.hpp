#ifndef EVYL_CORE_DEVICE_RESULT
#define EVYL_CORE_DEVICE_RESULT

#include <evyl/core/tools/common.hpp>

namespace EVY_NAMESPACE {
    enum EVY_RESULT_ENUM_NAME(Device) : uint8_t
    {
        eDeviceResultSuccess          = 0,
        eDeviceResultError            = 1,
        eDeviceResultUnsupportedQueue = 2
    };

    EVY_CREATE_ERROR_CATEGORY_HPP(Device);
} // namespace EVY_NAMESPACE

#endif