#include "result.hpp"

namespace EVY_NAMESPACE {
    EVY_CREATE_ERROR_CATEGORY_CPP(Device) {
        switch (CODE) {
            case eDeviceResultSuccess: return "No problems detected";
            case eDeviceResultError: return "A general error has occured";
            case eDeviceResultUnsupportedQueue: return "A requested queue is unsupported by the device";
            default: return "An unknown error has occured";
        }
    }
} // namespace EVY_NAMESPACE