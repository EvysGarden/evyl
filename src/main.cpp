#include <evyl/core/device/factory.hpp>
#include <evyl/core/instance/factory.hpp>

#include <iostream>

using namespace std;
using namespace evy;

int
main() {
    auto window = make_shared<Window>(200, 200, "yo");

    auto instanceResult = (*Instance::createFactory())
                              .setAppName("Evy's little engine")
                              .setAppVersion({ 1, 0, 0, 0 })
                              .setEngineName("Evy's little engine")
                              .setEngineVersion({ 1, 0, 0, 0 })
                              .addExtensions(window->getRequiredInstanceExtensions())
                              .enableDebugging(VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT,
                                               { VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT,
                                                 VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT })
                              .buildAndDestruct();
    if (!instanceResult) throw MonoException(instanceResult.error());
    shared_ptr<Instance> instance { instanceResult.value() };

    auto surface = make_shared<Surface>(window, instance);

    auto deviceResult = (*Device::createFactory())
                            .addValidationLayers()
                            .addExtension(VK_KHR_SWAPCHAIN_EXTENSION_NAME)
                            .buildAndDestruct(surface);
    if (!deviceResult) throw MonoException(deviceResult.error());
    shared<Device> device { deviceResult.value() };

    window->setVisible(true);
    while (!window->shouldClose() && !instance->interrupted()) { glfwPollEvents(); }
    cout << "stopping now" << endl;
}