# Evyl
#### The ~~evil~~ Evyl Vulkan Library

---

## cloning & building
```sh
# clone repository and step inside
git clone git@gitlab.fachschaften.org:smjlstor/evyl.git && cd evyl
# clone relevant submodules
git submodule update --init
# init vcpkg
./external/vcpkg/bootstrap-vcpkg.sh # optionally pass -disableMetrics to disable telemetry
# finally configure and build
mkdir build
cmake -B build -S .
cmake --build build
```
